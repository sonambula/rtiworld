/*
 * ChatListener.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: irene
 */

#ifndef CHATLISTENER_HPP_
#define CHATLISTENER_HPP_

#include <chat.hpp>
#include <dds/dds.hpp>
#include <string>
#include <iostream>
#include <DataStateLogger.hpp>

namespace RtiWorld {

/**
 * ChatReaderListener class
 *
 * This listener is going to be used to subscribe and manage the Chat samples sent by other players
 */
class ChatReaderListener: public dds::sub::DataReaderListener<Chat> {
private:
	std::string playerName;
	dds::pub::DataWriter<Chat> chatWriter;


	DataStateLogger dataStateLogger;
public:
	ChatReaderListener(std::string name, dds::pub::DataWriter<Chat>& chatWriter);
	virtual ~ChatReaderListener();

	/**
	 * @brief No-op
	 */
	virtual void on_requested_deadline_missed(dds::sub::DataReader<Chat>&,
			const dds::core::status::RequestedDeadlineMissedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_requested_incompatible_qos(dds::sub::DataReader<Chat>&,
			const dds::core::status::RequestedIncompatibleQosStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_sample_rejected(dds::sub::DataReader<Chat>&,
			const dds::core::status::SampleRejectedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_liveliness_changed(dds::sub::DataReader<Chat>&,
			const dds::core::status::LivelinessChangedStatus& status) {
		std::cout<<"ChatListener - DataReader: liveliness change. alive acount: " <<status.alive_count()<<std::endl;
	}

	/**
	 * @brief Receive Chats
	 */
	virtual void on_data_available(dds::sub::DataReader<Chat>&);

	/**
	 * @brief No-op
	 */
	virtual void on_subscription_matched(dds::sub::DataReader<Chat>& reader,
			const dds::core::status::SubscriptionMatchedStatus& status) {
		std::cout<<"ChatListener - DataReader: A subscription has been matched/unmatched. Current count: "<<status.current_count() <<std::endl;
	}

	/**
	 * @brief No-op
	 */
	virtual void on_sample_lost(dds::sub::DataReader<Chat>&,
			const dds::core::status::SampleLostStatus&) {
	}
};

} /* namespace RtiWorld */

#endif /* CHATLISTENER_HPP_ */
