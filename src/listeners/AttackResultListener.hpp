/*
 * AttackResultListener.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: irene
 */

#ifndef ATTACKRESULTLISTENER_HPP_
#define ATTACKRESULTLISTENER_HPP_

#include "AttackResult.hpp"
#include <dds/dds.hpp>
#include <string>

namespace RtiWorld {

/**
 * AttackResultListener class
 * the listener to be used to subscribe to a AttackResult topic.
 * This listener read the result of a previous attack did by the one that used it.
 * Updates the user status.
 */
class AttackResultListener: public dds::sub::DataReaderListener<AttackResult> {
private:
	Player& myPlayerData;
public:
	AttackResultListener(Player& player);
	virtual ~AttackResultListener();

	/**
	 * @brief No-op
	 */
	virtual void on_requested_deadline_missed(dds::sub::DataReader<AttackResult>&,
			const dds::core::status::RequestedDeadlineMissedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_requested_incompatible_qos(dds::sub::DataReader<AttackResult>&,
			const dds::core::status::RequestedIncompatibleQosStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_sample_rejected(dds::sub::DataReader<AttackResult>&,
			const dds::core::status::SampleRejectedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_liveliness_changed(dds::sub::DataReader<AttackResult>&,
			const dds::core::status::LivelinessChangedStatus&) {
	}

	/**
	 * Receive Chats
	 */
	virtual void on_data_available(dds::sub::DataReader<AttackResult>&);

	/**
	 * @brief No-op
	 */
	virtual void on_subscription_matched(dds::sub::DataReader<AttackResult>&,
			const dds::core::status::SubscriptionMatchedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_sample_lost(dds::sub::DataReader<AttackResult>&,
			const dds::core::status::SampleLostStatus&) {
	}
};

} /* namespace RtiWorld */

#endif /* ATTACKRESULTLISTENER_HPP_ */
