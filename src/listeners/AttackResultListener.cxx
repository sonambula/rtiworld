/*
 * AttackResultListener.cxx
 *
 *  Created on: Oct 11, 2017
 *      Author: irene
 */

#include "AttackResultListener.hpp"

#include <random>

namespace RtiWorld {

AttackResultListener::AttackResultListener(Player& player) :
		myPlayerData(player) {

}

AttackResultListener::~AttackResultListener() {
}

void AttackResultListener::on_data_available(
		dds::sub::DataReader<AttackResult>& data) {
	// Take all samples
	dds::sub::LoanedSamples<AttackResult> samples = data->take();
	for (auto sample : samples) {
		if (sample.info().valid()
				&& sample.data().playerNameFrom() != myPlayerData.name()
				&& sample.data().playerNameTo() == myPlayerData.name()) {

			switch (sample.data().result().underlying()) {
			case (AttackResultEnum::WIN):
				std::cout << "You win!" << std::endl;
				myPlayerData.level(myPlayerData.level() + 1);
				break;
			case (AttackResultEnum::LOSE):
				std::cout << "You lost!" << std::endl;
				myPlayerData.live(myPlayerData.live() - 10);
				if (myPlayerData.live() <= 0) {
					std::cout << "Ohh! you die!" << std::endl;
				}
				break;
			case (AttackResultEnum::PAIR):
				std::cout << "Nobody wins!" << std::endl;
				break;
			default: //Should be DIE_ENEMY
				std::cout << "Your enemy is already death..." << std::endl;
			}
		}
	}

}

} /* namespace RtiWorld */
