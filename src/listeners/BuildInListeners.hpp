/*
 * BuildInListeners.hpp
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#ifndef BUILDINLISTENERS_HPP_
#define BUILDINLISTENERS_HPP_
#include<dds/dds.hpp>
#include<DataStateLogger.hpp>
namespace RtiWorld {
/**
 * This listener its going to be used in the player node to know about the discovering status for the publication side.
 *
 * (Exercise 9)
 */
class MyBuiltInForPlayerListener: public dds::sub::NoOpDataReaderListener<dds::topic::PublicationBuiltinTopicData>
{
public:
	DataStateLogger logger;

	MyBuiltInForPlayerListener() {
	}

	virtual void on_data_available(dds::sub::DataReader<dds::topic::PublicationBuiltinTopicData>& reader) override
	{

		dds::sub::LoanedSamples<dds::topic::PublicationBuiltinTopicData> samples = reader.take();

		for (auto sample : samples) {
			if (sample.info().valid()) {
				std::cout << "Built-in Reader: PlayerNode Publication: topic " << sample.data().topic_name() << ", type ";
				std::cout << sample.data().type_name() << std::endl;
			} else {
				std::cout << "Built-in Reader: PlayerNode: Invalid data." << std::endl;
			}
			logger.log(sample.info());

		}
	}
};

/**
 * This listener its going to be used in the weather node to know about the discovering status for the subscription side.
 */
class MyBuiltInForWeatherListener: public dds::sub::NoOpDataReaderListener<dds::topic::SubscriptionBuiltinTopicData>
{
public:
	DataStateLogger logger;

	MyBuiltInForWeatherListener() {
	}

	virtual void on_data_available(dds::sub::DataReader<dds::topic::SubscriptionBuiltinTopicData>& reader) override
	{
		dds::sub::LoanedSamples<dds::topic::SubscriptionBuiltinTopicData> samples = reader.take();

		for (auto sample : samples) {
			if (sample.info().valid()) {
				std::cout << "Built-in Reader: WeatherNode Subscription: topic " << sample.data().topic_name() << ", type ";
				std::cout << sample.data().type_name() << std::endl;
			} else {
				std::cout << "Built-in Reader: WeatherNode: Invalid data." << std::endl;
			}
			logger.log(sample.info());

		}
	}
};
}
#endif /* BUILDINLISTENERS_HPP_ */
