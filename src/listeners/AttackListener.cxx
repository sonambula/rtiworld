/*
 * AttackListener.cxx
 *
 *  Created on: Oct 11, 2017
 *      Author: irene
 */

#include "AttackListener.hpp"

#include <random>

namespace RtiWorld {

AttackListener::AttackListener(Player& player,
		dds::pub::DataWriter<AttackResult>& writer) :
		myPlayerData(player), attackResultWriter(writer) {

}

AttackListener::~AttackListener() {
}

void AttackListener::on_data_available(dds::sub::DataReader<Attack>& data) {
	// Take all samples
	dds::sub::LoanedSamples<RtiWorld::Attack> samples = data->take();
	for (auto sample : samples) {
		if (sample.info().valid()
				&& sample.data().playerNameFrom() != myPlayerData.name()
				&& sample.data().playerNameTo() == myPlayerData.name()) {

			if(myPlayerData.live()<=0){
				publishResult(sample.data().attacker(),AttackResultEnum::DIE_ENEMY);
				return;
			}
			//TODO:fight begins!
			std::cout << sample.data().playerNameFrom() << " attacks you!"
					<< std::endl;
			//TODO:This could be implemented using a strategy pattern to has more types of fighting algorithms
			fight(sample.data().attacker());
		}
	}
}

void AttackListener::fight(const Player& player) {
	unsigned int myRes = diceRoll(myPlayerData.level());
	unsigned int enemyRes = diceRoll(player.level());

	if (myRes < enemyRes) {
		std::cout << "You lost!" << std::endl;
		myPlayerData.live(myPlayerData.live()-10);
		if (myPlayerData.live()<=0){
			std::cout<<"Ohh! you die!"<<std::endl;
		}
		publishResult(player, AttackResultEnum::WIN);

	} else if (myRes > enemyRes) {
		std::cout << "You win!" << std::endl;
		myPlayerData.level(myPlayerData.level()+1);
		publishResult(player, AttackResultEnum::LOSE);
	} else {
		std::cout << "Nobody wins!" << std::endl;
		publishResult(player, AttackResultEnum::PAIR);
	}

}

unsigned int AttackListener::diceRoll(const unsigned int numberOfRolls) {
	unsigned int res = 0;
	for (int i = 0; i < numberOfRolls; ++i) {
		// Seed with a real random value, if available
		std::random_device r;

		// Choose a random mean between 1 and 6
		std::default_random_engine e1(r());
		std::uniform_int_distribution<int> uniform_dist(1, 6);
		unsigned int mean = uniform_dist(e1);

		res = +mean;
	}

	return res;
}

void AttackListener::publishResult(const Player& player,
		AttackResultEnum result) {
	AttackResult res(myPlayerData.name(), player.name(), result);
	attackResultWriter.write(res);
}

} /* namespace RtiWorld */
