/*
 * ChatListener.cxx
 *
 *  Created on: Oct 11, 2017
 *      Author: irene
 */

#include "ChatListener.hpp"

namespace RtiWorld {
ChatReaderListener::ChatReaderListener(std::string name,
		dds::pub::DataWriter<RtiWorld::Chat>& writer) :
		playerName(name), chatWriter(writer) {

}

ChatReaderListener::~ChatReaderListener() {
}

void ChatReaderListener::on_data_available(dds::sub::DataReader<Chat>& data) {
	// Take all samples
	std::cout<<" ChatListener::on_data_available "<<std::endl;
	dds::sub::LoanedSamples<RtiWorld::Chat> samples = data->take();
	for (auto sample : samples) {
		dataStateLogger.log(sample.info());
		if (sample.info().valid() && sample.data().playerNameFrom() != playerName
				&& (sample.data().playerNameTo() == playerName
						|| sample.data().playerNameTo() == "#all")) {
			std::cout << sample.data().playerNameFrom() << " says: "
					<< sample.data().message() << std::endl;

			if (sample.data().playerNameTo() == "#all" && sample.data().message()!="Welcome!") {
				RtiWorld::Chat sampleChat;
				sampleChat.playerNameTo(sample.data().playerNameFrom());
				sampleChat.playerNameFrom(playerName);
				sampleChat.message("Welcome!");

				chatWriter.write(sampleChat);
			}
		}
	}
}

} /* namespace RtiWorld */
