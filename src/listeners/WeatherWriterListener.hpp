#ifndef WEATHERWRITERLISTENER_HPP_
#define WEATHERWRITERLISTENER_HPP_
#include <dds/dds.hpp>
#include <string>
#include <iostream>
#include <WeatherStatus.hpp>
namespace RtiWorld {
class WeatherWriterListener: public dds::pub::NoOpDataWriterListener<WeatherStatus> {
	/**
	 * @brief the reader has a incompatible qos
	 */
	virtual void on_offered_incompatible_qos(
			dds::pub::DataWriter<WeatherStatus>& dw,
			const dds::core::status::OfferedIncompatibleQosStatus& status)
			{
		std::cout<<"Datareader with incompatible QoS policy: "<< status->last_policy_id()<<std::endl;
	}

	/**
	 * @brief A reader matched
	 */
	virtual void on_publication_matched(
			dds::pub::DataWriter<WeatherStatus>& dw,
			const dds::core::status::PublicationMatchedStatus& status)
			{
		std::cout<<"A new Reader has been matched: "<< status->current_count()<<std::endl;
	}

};
}

#endif /* WEATHERWRITERLISTENER_HPP_ */
