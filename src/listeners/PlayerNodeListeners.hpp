#ifndef PLAYERNODELISTENERS_HPP_
#define PLAYERNODELISTENERS_HPP_
#include <dds/dds.hpp>
#include <string>
#include <iostream>
#include <chat.hpp>
namespace RtiWorld {
class ChatWriterListener: public dds::pub::NoOpDataWriterListener<Chat> {
	virtual void on_publication_matched(dds::pub::DataWriter<Chat>& dwriter, const dds::core::status::PublicationMatchedStatus& status)
			{
		std::cout << "ChatWriterListener::on_publication_matched: DataWriter - publication matched/unmatched: " << status.current_count() << std::endl;
	}

};

class PlayerSubscriberListener: public dds::sub::NoOpSubscriberListener {

};
class PlayerPublisherListener: public dds::pub::NoOpPublisherListener {

};

class ChatTopicListener: public dds::topic::NoOpTopicListener<Chat> {

};

class MyParticipantListener: public dds::domain::NoOpDomainParticipantListener {
	virtual void on_publication_matched(dds::pub::AnyDataWriter& writer, const ::dds::core::status::PublicationMatchedStatus& status)
	{
		std::cout << "MyParticipantListener::on_publication_matched: Participant - publication matched/unmatched: " << writer.topic_name() << " "
				<< status.current_count() << std::endl;

	}
	virtual void on_data_on_readers(dds::sub::Subscriber& sub)
	{
		if (sub.status_changes() == (dds::core::status::StatusMask::data_available() & dds::core::status::StatusMask::data_on_readers()) ){
			std::cout << "MyParticipantListener::on_data_on_readers: A new data in the data readers and available:" << sub.instance_handle() << std::endl;
			sub.notify_datareaders();
		}
	}

};

}

#endif /* PLAYERNODELISTENERS_HPP_ */
