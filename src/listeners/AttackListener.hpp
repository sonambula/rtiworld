/*
 * AttackListener.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: irene
 */

#ifndef ATTACKLISTENER_HPP_
#define ATTACKLISTENER_HPP_

#include "Attack.hpp"
#include "AttackResult.hpp"
#include <dds/dds.hpp>
#include <string>

namespace RtiWorld {

/**
 * AttackListener class
 * the listener to be used to subscribe to a Attack topic.
 * In this listener all the logic of the fight has been developed. The result will be sent to the attacker from here to
 * let it know if he wins or loses.
 * Updates the user status.
 */
class AttackListener: public dds::sub::DataReaderListener<Attack> {
private:
	Player& myPlayerData;
	dds::pub::DataWriter<AttackResult> attackResultWriter;

	void fight(const Player& player);
	unsigned int diceRoll(const unsigned int numberOfRolls);
	void publishResult(const Player& player, const AttackResultEnum result);
public:
	AttackListener(Player& player, dds::pub::DataWriter<AttackResult>& writer);
	virtual ~AttackListener();

	/**
	 * @brief No-op
	 */
	virtual void on_requested_deadline_missed(dds::sub::DataReader<Attack>&,
			const dds::core::status::RequestedDeadlineMissedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_requested_incompatible_qos(dds::sub::DataReader<Attack>&,
			const dds::core::status::RequestedIncompatibleQosStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_sample_rejected(dds::sub::DataReader<Attack>&,
			const dds::core::status::SampleRejectedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_liveliness_changed(dds::sub::DataReader<Attack>&,
			const dds::core::status::LivelinessChangedStatus&) {
	}

	/**
	 * Receive Chats
	 */
	virtual void on_data_available(dds::sub::DataReader<Attack>&);

	/**
	 * @brief No-op
	 */
	virtual void on_subscription_matched(dds::sub::DataReader<Attack>&,
			const dds::core::status::SubscriptionMatchedStatus&) {
	}

	/**
	 * @brief No-op
	 */
	virtual void on_sample_lost(dds::sub::DataReader<Attack>&,
			const dds::core::status::SampleLostStatus&) {
	}
};

} /* namespace RtiWorld */

#endif /* ATTACKLISTENER_HPP_ */
