/*
 * WeatherNode.h
 *
 *  Created on: Oct 20, 2017
 *      Author: irene
 */

#ifndef WEATHERNODE_H_
#define WEATHERNODE_H_

#include<WeatherStatus.hpp>

namespace RtiWorld {

class WeatherNode {
public:
	const static unsigned short WEATHER_CONDITION_SIZE = 5;
	WeatherStatus wStatus;
	WeatherNode();
	virtual ~WeatherNode();

	bool checkWeatherConditions();
	WeatherStatus weatherStatus(){
		return this->wStatus;
	}
};

} /* namespace RtiWorld */

#endif /* WEATHERNODE_H_ */
