/*
 * PlayerNode.hpp
 *
 *  Created on: Oct 9, 2017
 *      Author: irene
 */

#ifndef PLAYERNODE_H_
#define PLAYERNODE_H_
#include <dds/dds.hpp>

#include <string>
#include <vector>
#include <memory>

#include <Attack.hpp>
#include <AttackResult.hpp>
#include <player.hpp>
#include <chat.hpp>
#include <WeatherStatus.hpp>
#include <IWeatherConditionSubscriber.hpp>

namespace RtiWorld {
/**
 * The Player Node class
 * All the main logic for the player node is here.
 *
 * The DDS subscription and writers needed has been created and managed from here.
 * Chat, Attack and AttackResults topics are received and sent.
 * WeatherCondition topics are received using the collaborator class (extended from IWeatherConditionSubscriber) instance weatherConditionSubscriber.
 * This collaborator should be passed as a parameter during the construction of the class using Inversion of Control.
 *
 * The main method to make it running has been created in the cpp file.
 */
class PlayerNode {
public:
	PlayerNode(int domainId,std::unique_ptr<IWeatherConditionSubscriber> subscriber);
	virtual ~PlayerNode();

	void publishMessageTo(const std::string to, const std::string message);
	void publishAttackTo(const std::string to);
	void printStatus();
private:
	Player data;
	int domainId;
	std::vector<dds::core::InstanceHandle> registerMapChat;

	dds::domain::DomainParticipant participant;
	dds::pub::Publisher publisher;
	dds::sub::Subscriber subscriber;


	dds::topic::Topic<Chat> chatTopic;
	dds::pub::DataWriter<Chat> chatWriter;
	dds::sub::DataReader<Chat> chatReader;

	dds::topic::Topic<Attack> attackTopic;
	dds::pub::DataWriter<Attack> attackWriter;
	dds::sub::DataReader<Attack> attackReader;

	dds::topic::Topic<AttackResult> attackResultTopic;
	dds::pub::DataWriter<AttackResult>* attackResultWriter;
	dds::sub::DataReader<AttackResult>* attackResultReader;

	std::unique_ptr<IWeatherConditionSubscriber> weatherConditionSubscriber;

	void subscribeToRelevantInfo();
	bool isAlive();
	void checkWeatherConditionPolling();
	void checkWeatherConditionWaitset();
};

}

#endif /* PLAYERNODE_H_ */
