/*
 * PlayerNode.cxx
 *
 *  Created on: Oct 9, 2017
 *      Author: irene
 */

#include "PlayerNode.hpp"
#include <ChatListener.hpp>
#include <AttackListener.hpp>
#include <AttackResultListener.hpp>
#include <PlayerNodeListeners.hpp>
#include <thread>
#include <iostream>
#include <WeatherConditionSubscriberByWaitset.hpp>
#include <WeatherConditionSubscriberByPolling.hpp>
#include <WeatherConditionFilteredSubscriber.hpp>
#include <WeatherConditionQuerySubscriber.hpp>
#include <BuildInListeners.hpp>
#include <BuiltInTopicManager.hpp>
using namespace RtiWorld;

PlayerNode::PlayerNode(int domainId, std::unique_ptr<IWeatherConditionSubscriber> wsubscriber) :
		participant(dds::domain::DomainParticipant(domainId,
				dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::reliableProfile")->participant_qos())),
				publisher(dds::pub::Publisher(participant)),
				subscriber(dds::sub::Subscriber(participant)),
				chatTopic(dds::topic::Topic<Chat>(participant, "WarriorChat")),
				chatWriter(publisher, chatTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "chatLibrary::chatProfile")->datawriter_qos()),
				chatReader(subscriber, chatTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "chatLibrary::chatProfile")->datareader_qos()),
				attackTopic(dds::topic::Topic<Attack>(participant, "attack")),
				attackWriter(publisher, attackTopic,
						dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortProfile")->datawriter_qos()),
				attackReader(subscriber, attackTopic,
						dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortProfile")->datareader_qos()),
				attackResultTopic(dds::topic::Topic<AttackResult>(participant, "attackResult"))
{

	this->data.level(1);
	this->data.live(100);
	this->domainId = domainId;
	this->weatherConditionSubscriber = std::move(wsubscriber);

	//Exercise 2aii: changing the QoS policy from attackResult to admit late joiners
	dds::core::policy::Durability dTransient;
	dTransient.kind(dds::core::policy::DurabilityKind::TRANSIENT);
	dds::core::QosProvider base("src/USER_QOS_PROFILES.xml",
			"defaultLibrary::reliableProfile");
	dds::sub::qos::DataReaderQos baseReader = base.datareader_qos()
			<< dTransient;
	dds::pub::qos::DataWriterQos baseWriter = base.datawriter_qos()
			<< dTransient;

	attackResultReader = new dds::sub::DataReader<AttackResult>(subscriber,
			attackResultTopic, baseReader);
	attackResultWriter = new dds::pub::DataWriter<AttackResult>(publisher,
			attackResultTopic, baseWriter);

	//Exercise 6
	participant->add_peer("udpv6://");
	std::cout << "initial peers:";
	for (std::string seq : participant.qos()->discovery.initial_peers())
		std::cout << seq << ",";
	std::cout << "." << std::endl;

	//Welcome and player's name
	std::cout << "Welcome to RTI World! Please, select a name for your character." << std::endl;
	std::string name;
	std::getline(std::cin, name);
	this->data.name(name);

	printStatus();
	subscribeToRelevantInfo();
	weatherConditionSubscriber->execute(domainId);
	publishMessageTo("#all", "I am here!");

}

PlayerNode::~PlayerNode() {

	while (!registerMapChat.empty()) {
		dds::core::InstanceHandle handle =
				registerMapChat[registerMapChat.size() - 1];
		std::cout << "unregister instance " << handle << std::endl;
		chatWriter.unregister_instance(handle);
		registerMapChat.pop_back();
	}

	chatWriter.close();
	chatReader.close();
	chatTopic.close();

	attackResultWriter->close();
	attackResultReader->close();
	attackResultTopic.close();

	publisher.close();
	subscriber.close();
	participant.close();

	if (attackResultReader != NULL)
		delete attackResultReader;
	if (attackResultWriter != NULL)
		delete attackResultWriter;
}

void PlayerNode::printStatus() {
	std::cout << "Hi, " << this->data.name() << "! your level is "
			<< this->data.level() << " and your live is: " << this->data.live()
			<< std::endl;
}

void PlayerNode::publishMessageTo(const std::string to,
		const std::string message) {
	if (data.name() == to) {
		std::cout << "Are You trying to talk to yourself? really?" << std::endl;
		return;
	}

	RtiWorld::Chat sample;
	sample.playerNameTo(to);
	sample.playerNameFrom(this->data.name());
	sample.message(message);

	if (chatWriter.lookup_instance(sample).is_nil()) {
		std::cout << "registering instance " << sample << std::endl;
		registerMapChat.push_back(chatWriter.register_instance(sample));
	}

	chatWriter.write(sample);

}

void PlayerNode::publishAttackTo(const std::string to) {
	if (data.name() == to) {
		std::cout << "You punch yourself in the face... ouch!" << std::endl;
		return;
	}

	if (isAlive()) {
		std::cout << "You are dead, you cannot fight with anyone!"
				<< std::endl;
		return;
	}

	Attack attackSample(data.name(), to, data);

	attackWriter.write(attackSample);

}

bool PlayerNode::isAlive() {
	return data.live() <= 0;
}

void PlayerNode::subscribeToRelevantInfo() {
	//Exercise 5: adding listeners
	participant.listener(new MyParticipantListener(), dds::core::status::StatusMask::publication_matched() | dds::core::status::StatusMask::data_on_readers());
	publisher.listener(new PlayerPublisherListener(), dds::core::status::StatusMask::none());
	subscriber.listener(new PlayerSubscriberListener(), dds::core::status::StatusMask::none());
	chatWriter.listener(new ChatWriterListener(), dds::core::status::StatusMask::publication_matched());

	chatReader.listener(new ChatReaderListener(this->data.name(), chatWriter),
			dds::core::status::StatusMask::all());
	attackReader.listener(new AttackListener(data, *attackResultWriter),
			dds::core::status::StatusMask::all());
	attackResultReader->listener(new AttackResultListener(data),
			dds::core::status::StatusMask::all());

}

/**
 * Main method to run a PlayerNode.
 *
 * The PlayerNode class is the one with all the logic implemented.
 * This method implements the console input/output to comunicate with the player.
 *
 * The player can:
 *  - 'exit's the application
 *  - 'chat's with other players
 *  - 'attack's other player and start a fight
 *  - sees 'who' are in the game
 *  - checks his 'status'
 *  - asks for 'help' to know the possible commands
 */
int main(int argc, char *argv[]) {
	int domainId = 0;

	if (argc >= 2) {
		domainId = atoi(argv[1]);
	}

	/* Uncomment this to turn on additional logging
	 NDDSConfigLogger::get_instance()->
	 set_verbosity_by_category(NDDS_CONFIG_LOG_CATEGORY_API,
	 NDDS_CONFIG_LOG_VERBOSITY_STATUS_ALL);
	 */

	//Exercise 4
	//std::unique_ptr<IWeatherConditionSubscriber> localWeatherSub(new WeatherConditionSubscriberByWaitset());
	std::unique_ptr<IWeatherConditionSubscriber> localWeatherSub(new WeatherConditionSubscriberByPolling());

	//Exercise 9
	//std::unique_ptr<IWeatherConditionSubscriber> localWeatherSub(new WeatherConditionFilteredSubscriber());
	//std::unique_ptr<IWeatherConditionSubscriber> localWeatherSub(new WeatherConditionQuerySubscriber());
	PlayerNode player(domainId, std::move(localWeatherSub));

	//Exercise 8 built-in topics: subscribing and registering specific listener to built-in topics.
	BuiltInTopicManager<dds::topic::PublicationBuiltinTopicData> builtInTopicManager(domainId);
	MyBuiltInForPlayerListener builtinDataReaderListener;
	builtInTopicManager.subscribe(builtinDataReaderListener);

	//TODO: use command pattern better than that...
	std::string command;
	while (command != "exit") {
		std::cout << ">> ";
		std::getline(std::cin, command);
		if (command == "chat") {
			std::cout << "Write the name of the warrior to talk to him."
					<< std::endl;
			std::string warriorName;
			std::getline(std::cin, warriorName);

			std::cout << "What would you like to tell him?" << std::endl;
			std::string message;
			std::getline(std::cin, message);

			player.publishMessageTo(warriorName, message);
		} else if (command == "fight") {
			std::cout << "Write the name of the warrior to fight with him."
					<< std::endl;
			std::string warriorName;
			std::getline(std::cin, warriorName);

			player.publishAttackTo(warriorName);
		} else if (command == "status") {
			player.printStatus();
		} else if (command == "who") {
			player.publishMessageTo("#all", "I am here!");
		} else if (command == "help") {
			std::cout
					<< "For now, you can know your status, find others, chat with friends or fight against enemies. Just type the works 'status', 'who', 'chat' or 'fight', press 'Enter' and let see what happen next... Type 'exit' to kill this app."
					<< std::endl;
		}
	}
	return 0;
}
