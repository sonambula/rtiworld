/*
 * WeatherConditionSubscriptorByWaitset.hpp
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#ifndef WEATHERCONDITIONSUBSCRIPTORBYWAITSET_HPP_
#define WEATHERCONDITIONSUBSCRIPTORBYWAITSET_HPP_

#include "IWeatherConditionSubscriber.hpp"
#include <dds/dds.hpp>

namespace RtiWorld {

/**
 * WeatherConditionSubscriberByPolling class
 *
 * This class allow to subscribe to the WeatherCondition topic and obtain the data by using conditions and waitset.
 * (Exercise 4c)
 */
class WeatherConditionSubscriberByWaitset: public IWeatherConditionSubscriber {
public:

	void execute(unsigned short domainId) override;
};

} /* namespace RtiWorld */

#endif /* WEATHERCONDITIONSUBSCRIPTORBYWAITSET_HPP_ */
