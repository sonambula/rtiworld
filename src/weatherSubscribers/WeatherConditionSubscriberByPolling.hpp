/*
 * WeatherConditionSubscriptorByPolling.hpp
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#ifndef WEATHERCONDITIONSUBSCRIPTORBYPOLLING_HPP_
#define WEATHERCONDITIONSUBSCRIPTORBYPOLLING_HPP_

#include "IWeatherConditionSubscriber.hpp"

namespace RtiWorld {
/**
 * WeatherConditionSubscriberByPolling class
 *
 * This class allow to subscribe to the WeatherCondition topic and obtain the data doing polling.
 * (Exercise 4b)
 */
class WeatherConditionSubscriberByPolling: public IWeatherConditionSubscriber {
public:

	void execute(unsigned short domainId) override;
};

} /* namespace RtiWorld */

#endif /* WEATHERCONDITIONSUBSCRIPTORBYPOLLING_HPP_ */
