/*
 * WeatherConditionFilteredSubscriber.hpp
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#ifndef WEATHERCONDITIONQUERYSUBSCRIPTOR_HPP_
#define WEATHERCONDITIONQUERYSUBSCRIPTOR_HPP_

#include "IWeatherConditionSubscriber.hpp"
#include <dds/dds.hpp>

namespace RtiWorld {

/**
 * WeatherConditionQuerySubscriber class
 *
 * This class allow to subscribe to the WeatherCondition topic, filtering the sent data using a Query condition.
 * (Exercise 9)
 */
class WeatherConditionQuerySubscriber: public IWeatherConditionSubscriber {
public:

	void execute(unsigned short domainId) override;
};

} /* namespace RtiWorld */

#endif /* WEATHERCONDITIONQUERYSUBSCRIPTOR_HPP_ */
