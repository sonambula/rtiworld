/*
 * WeatherConditionSubscriptorByPolling.cxx
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#include "WeatherConditionSubscriberByPolling.hpp"
#include <dds/dds.hpp>
#include <thread>
#include <iostream>
#include <WeatherStatus.hpp>

using namespace RtiWorld;

void WeatherConditionSubscriberByPolling::execute(const unsigned short domainId) {
	std::thread t(
			[domainId]() {
				const std::chrono::seconds timeToUpdateInSec = std::chrono::seconds(1);
				dds::domain::DomainParticipant participant(domainId,dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortProfile")->participant_qos());
				dds::topic::Topic<WeatherStatus> weatherTopic(participant, "WeatherCondition");
				dds::sub::Subscriber subscriber(participant);
				dds::sub::DataReader<WeatherStatus> reader(subscriber, weatherTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml","defaultLibrary::bestEffortProfile")->datareader_qos());

				while(true)
				{
					dds::sub::LoanedSamples<WeatherStatus> samples = reader->take();
					for (auto sample : samples) {
						if (sample.info().valid())
						{
							std::cout<<"The weather condition are "<<sample.data().status()<<" "<<sample.data().celsiusDegrees()<<" C"<<std::endl;
						}
					}
					std::this_thread::sleep_for(timeToUpdateInSec);
				}

			});
	t.detach();

} /* namespace RtiWorld */
