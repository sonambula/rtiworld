#ifndef IWEATHERCONDITIONSUBSCRIPTOR
#define IWEATHERCONDITIONSUBSCRIPTOR
namespace RtiWorld{
/**
 * Abstract class to be extended in order to provide a way to subscribe and read the samples for the WeatherStatus
 */
class IWeatherConditionSubscriber{
public:
	virtual ~IWeatherConditionSubscriber(){}
	virtual void execute(const unsigned short domainId){};
};
}

#endif //IWEATHERCONDITIONSUBSCRIPTOR
