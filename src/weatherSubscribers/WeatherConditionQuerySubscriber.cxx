/*
 * WeatherConditionFilteredSubscriber.cxx
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#include "WeatherConditionQuerySubscriber.hpp"
#include <dds/dds.hpp>
#include <thread>
#include <iostream>
#include<WeatherStatus.hpp>
#include<BuildInListeners.hpp>

using namespace RtiWorld;

void WeatherConditionQuerySubscriber::execute(const unsigned short domainId) {
	std::thread t(
			[domainId]() {
				const std::chrono::seconds timeToUpdateInSec = std::chrono::seconds(1);
				dds::domain::DomainParticipant participant(domainId, dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortMulticastProfile")->participant_qos());
				dds::topic::Topic<WeatherStatus> weatherTopic(participant, "WeatherCondition");

				dds::sub::Subscriber subscriber(participant);
				dds::sub::DataReader<WeatherStatus> reader(subscriber, weatherTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml","defaultLibrary::bestEffortMulticastProfile")->datareader_qos());
				//Exercise 3b, incompatible data reader QoS
				//dds::sub::DataReader<WeatherStatus> reader(subscriber, weatherTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml","defaultLibrary::reliableProfile")->datareader_qos());

				//Exercise 9: Filtering in the datawriter, datareader communicate its datawriter only to send the filtered data
				// Create the parameter list
				std::vector<std::string> queryParameters(1);
				queryParameters[0] = "'CLOUDY'";
				dds::sub::cond::QueryCondition query ( dds::sub::Query(reader, "status = %0", queryParameters),
						dds::sub::status::DataState::new_data());

				dds::core::cond::StatusCondition statusCondition(reader);
				statusCondition.enabled_statuses(dds::core::status::StatusMask::data_available());

				dds::sub::cond::ReadCondition read_condition(reader, dds::sub::status::DataState::new_data(),[&reader,&queryParameters,&query]()
						{
							dds::sub::LoanedSamples<WeatherStatus> samples = reader.select().condition(query).take();
							for (auto sample : samples) {
								if (sample.info().valid())
								{
									std::cout<<"EYYYYYY!!!!! is "<<sample.data().status()<<" and the temp is "<<sample.data().celsiusDegrees()<<" C"<<std::endl;
									queryParameters[0] = "'SUNNY'";
									query.parameters(queryParameters.begin(),queryParameters.end());
								}
							}
						});
				dds::core::cond::WaitSet waitset;
				waitset+=statusCondition;
				waitset += read_condition;
				while(true) {
					waitset.dispatch(dds::core::Duration(2));
				}

			});
	t.detach();
}

