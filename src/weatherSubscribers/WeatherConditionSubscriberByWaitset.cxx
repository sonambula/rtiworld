/*
 * WeatherConditionSubscriptorByWaitset.cxx
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#include "WeatherConditionSubscriberByWaitset.hpp"
#include <dds/dds.hpp>
#include <thread>
#include <iostream>
#include<WeatherStatus.hpp>
#include<BuildInListeners.hpp>

using namespace RtiWorld;

void WeatherConditionSubscriberByWaitset::execute(const unsigned short domainId) {
	std::thread t(
			[domainId]() {
				const std::chrono::seconds timeToUpdateInSec = std::chrono::seconds(1);
				dds::domain::DomainParticipant participant(domainId, dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortMulticastProfile")->participant_qos());
				dds::topic::Topic<WeatherStatus> weatherTopic(participant, "WeatherCondition");
				dds::sub::Subscriber subscriber(participant);
				dds::sub::DataReader<WeatherStatus> reader(subscriber, weatherTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml","defaultLibrary::bestEffortMulticastProfile")->datareader_qos());
				//Exercise 3b, incompatible data reader QoS
				//dds::sub::DataReader<WeatherStatus> reader(subscriber, weatherTopic, dds::core::QosProvider("src/USER_QOS_PROFILES.xml","defaultLibrary::reliableProfile")->datareader_qos());

				dds::core::cond::StatusCondition statusCondition(reader);
								statusCondition.enabled_statuses(dds::core::status::StatusMask::data_available());

				dds::sub::cond::ReadCondition read_condition(reader, dds::sub::status::DataState::new_data(),[&reader]()
						{
							dds::sub::LoanedSamples<WeatherStatus> samples = reader->take();
							for (auto sample : samples) {
								if (sample.info().valid())
								{
									std::cout<<"The weather condition are change: "<<sample.data().status()<<" "<<sample.data().celsiusDegrees()<<" C"<<std::endl;
								}
							}
						});
				dds::core::cond::WaitSet waitset;
				waitset+=statusCondition;
				waitset += read_condition;
				while(true) {
					waitset.dispatch(dds::core::Duration(2));
				}

			});
	t.detach();
}

