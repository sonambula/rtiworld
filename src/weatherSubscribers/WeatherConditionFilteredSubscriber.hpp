/*
 * WeatherConditionFilteredSubscriber.hpp
 *
 *  Created on: Oct 25, 2017
 *      Author: irene
 */

#ifndef WEATHERCONDITIONFILTEREDSUBSCRIPTOR_HPP_
#define WEATHERCONDITIONFILTEREDSUBSCRIPTOR_HPP_

#include "IWeatherConditionSubscriber.hpp"
#include <dds/dds.hpp>

namespace RtiWorld {

/**
 * WeatherConditionFilteredSubscriber class
 *
 * This class allow to subscribe to the WeatherCondition topic, filtering the arrived data using a Content Filter Topic.
 * (Exercise 9)
 */
class WeatherConditionFilteredSubscriber: public IWeatherConditionSubscriber {
public:

	void execute(unsigned short domainId) override;
};

} /* namespace RtiWorld */

#endif /* WEATHERCONDITIONFILTEREDSUBSCRIPTOR_HPP_ */
