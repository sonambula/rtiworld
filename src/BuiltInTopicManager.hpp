/*
 * BuiltInTopicManager.h
 *
 *  Created on: Oct 26, 2017
 *      Author: irene
 */

#ifndef BUILTINTOPICMANAGER_H_
#define BUILTINTOPICMANAGER_H_
#include <dds/dds.hpp>
namespace RtiWorld {

/**
 * BuiltInTopicManager class template
 * This class template can be used to subscribe to the built-in topics in order to
 * monitoring what is going on during the discovery.
 *
 * (Exercise 8)
 */
template <class T>
class BuiltInTopicManager {
private:
	dds::domain::DomainParticipant participant;
	dds::sub::Subscriber builtinSubscriber;
	std::vector<dds::sub::DataReader<T>> builtinDataReaders;
public:
	BuiltInTopicManager(unsigned short domainId);
	virtual ~BuiltInTopicManager();

	/**
	 * Subscribe to the built-in topic using a specific listener to deal with the received data.
	 * If a listener is already set and you call this method with a different ones, the previous listener will be overwritten.
	 * @param listener the listener to be used
	 * @return bool if the listener could be set or not.
	 */
	bool subscribe(dds::sub::DataReaderListener<T>& listener);
};

} /* namespace RtiWorld */

#endif /* BUILTINTOPICMANAGER_H_ */
