/*
 * BuiltInTopicManager.cpp
 *
 *  Created on: Oct 26, 2017
 *      Author: irene
 */

#include "BuiltInTopicManager.hpp"

namespace RtiWorld{
template<class T>
BuiltInTopicManager<T>::BuiltInTopicManager(unsigned short domainId) :
		participant(dds::domain::DomainParticipant(domainId)), builtinSubscriber(dds::sub::builtin_subscriber(participant))
{

}
template<class T>
BuiltInTopicManager<T>::~BuiltInTopicManager()
{
}
template<class T>
bool BuiltInTopicManager<T>::subscribe(dds::sub::DataReaderListener<T>& listener) {
	//Exercise 8 built-in topics: subscribing and registering specific listener to built-in topics.
	//Find the datareader from the subscriber
	dds::sub::find<dds::sub::DataReader<T>>(builtinSubscriber, dds::topic::publication_topic_name(),
			std::back_inserter(builtinDataReaders));
	if (builtinDataReaders.size() != 0) {
		builtinDataReaders[0].listener(&listener, dds::core::status::StatusMask::all());
		return true;
	}
	return false;
}


template class BuiltInTopicManager<dds::topic::PublicationBuiltinTopicData>;
template class BuiltInTopicManager<dds::topic::SubscriptionBuiltinTopicData>;
}
