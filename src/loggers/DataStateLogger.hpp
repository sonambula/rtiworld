/*
 * DataStateLogger.h
 *
 *  Created on: Oct 17, 2017
 *      Author: irene
 */

#ifndef DATASTATELOGGER_H_
#define DATASTATELOGGER_H_

#include <dds/dds.hpp>

namespace RtiWorld {

class DataStateLogger {
public:
	DataStateLogger();
	virtual ~DataStateLogger();

	void log(const dds::sub::SampleInfo& sample);
};

} /* namespace RtiWorld */

#endif /* DATASTATELOGGER_H_ */
