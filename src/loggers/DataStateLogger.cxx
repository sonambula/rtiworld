/*
 * DataStateLogger.cpp
 *
 *  Created on: Oct 17, 2017
 *      Author: irene
 */

#include <DataStateLogger.hpp>
#include <iostream>

using namespace RtiWorld;

DataStateLogger::DataStateLogger() {
	// TODO Auto-generated constructor stub

}

DataStateLogger::~DataStateLogger() {
	// TODO Auto-generated destructor stub
}

void DataStateLogger::log(const dds::sub::SampleInfo& sample) {
	dds::sub::status::DataState state = sample.state();
	std::cout<<"DataStateLogger::log - A new data arrived from "<<sample.instance_handle()<<" and its status is: ";
	std::cout<<(sample.valid()?"valid data":"INvalid data");

	std::cout<<" The Instance State is: ";
	if(state.instance_state() == DDS_ALIVE_INSTANCE_STATE) std::cout<<"Alive ";
	if(state.instance_state() == DDS_NOT_ALIVE_DISPOSED_INSTANCE_STATE) std::cout<<"Gracefully termination ";
	if(state.instance_state() == DDS_NOT_ALIVE_NO_WRITERS_INSTANCE_STATE) std::cout<<"Ungracefully termination ";

	std::cout<<std::endl;

}
