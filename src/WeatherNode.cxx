/*
 * WeatherNode.cpp
 *
 *  Created on: Oct 20, 2017
 *      Author: irene
 */

#include "WeatherNode.hpp"
#include <random>
#include <thread>
#include <iostream>
#include <chrono>
#include <dds/dds.hpp>
#include <WeatherWriterListener.hpp>
#include <BuildInListeners.hpp>
#include <BuiltInTopicManager.hpp>

using namespace RtiWorld;

WeatherNode::WeatherNode() {

}

WeatherNode::~WeatherNode() {
}

bool WeatherNode::checkWeatherConditions() {
	// Seed with a real random value, if available
	std::random_device r;

	std::default_random_engine e1(r());
	if (std::uniform_int_distribution<int>(1, 2)(e1) == 1) {
		std::cout << "The weather condition are changing from " << wStatus.status() << " " << wStatus.celsiusDegrees() << "C to ";
		// Choose a random mean between 1 and WEATHER_CONDITION_SIZE to select the new status
		std::uniform_int_distribution<int> uniform_dist(0,
				WEATHER_CONDITION_SIZE - 1);
		WeatherStatusEnum_def def;
		wStatus.status(WeatherStatusEnum(WeatherStatusEnum_def::type(uniform_dist(e1))));

		//Randomly increment or decrement the temperature
		if (std::uniform_int_distribution<int>(1, 2)(e1) == 1) {
			wStatus.celsiusDegrees(wStatus.celsiusDegrees() + 1);
		} else {
			wStatus.celsiusDegrees(wStatus.celsiusDegrees() - 1);
		}

		std::cout << wStatus.status() << " " << wStatus.celsiusDegrees() << "C" << std::endl;
		return true;
	}
	return false;
}

int main(int argc, char *argv[]) {
	int domainId = 0;

	if (argc >= 2) {
		domainId = atoi(argv[1]);
	}

	std::thread t(
			[domainId]() {
			const std::chrono::seconds timeToUpdateInSec = std::chrono::seconds(5);
			WeatherNode wNode;
			dds::domain::DomainParticipant participant(domainId,
					dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortMulticastProfile")->participant_qos());
			dds::topic::Topic<WeatherStatus> weatherTopic(participant, "WeatherCondition");
			dds::pub::Publisher publisher(participant);
			dds::pub::DataWriter<WeatherStatus> dataWriter(publisher, weatherTopic,
					dds::core::QosProvider("src/USER_QOS_PROFILES.xml", "defaultLibrary::bestEffortMulticastProfile")->datawriter_qos());
			WeatherWriterListener listener;
			dataWriter.listener(&listener, dds::core::status::StatusMask::all());

			//Exercise 8 built-in topics: subscribing and registering specific listener to built-in topics.
			BuiltInTopicManager<dds::topic::SubscriptionBuiltinTopicData> builtInTopicManager(domainId);
			MyBuiltInForWeatherListener builtinDataReaderListener;
			builtInTopicManager.subscribe(builtinDataReaderListener);

			while(true)
			{
				if (wNode.checkWeatherConditions()) dataWriter.write(wNode.weatherStatus());

				std::this_thread::sleep_for(timeToUpdateInSec);
			}
		});
	t.detach();

	std::cout << "Weather Module started. Type 'exit' to close." << std::endl;
	std::string c;
	while (c != "exit") {
		std::getline(std::cin, c);
	}

	return 0;
}

