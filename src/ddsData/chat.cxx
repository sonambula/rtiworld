

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from chat.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>

#include "rti/topic/cdr/Serialization.hpp"

#include "chat.hpp"
#include "chatPlugin.hpp"

#include <rti/util/ostream_operators.hpp>

namespace RtiWorld {

    // ---- Chat: 

    Chat::Chat()  {
    }   

    Chat::Chat (
        const std::string& playerNameTo,
        const std::string& playerNameFrom,
        const std::string& message)
        :
            m_playerNameTo_( playerNameTo ),
            m_playerNameFrom_( playerNameFrom ),
            m_message_( message ) {
    }

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Chat::Chat(Chat&& other_) OMG_NOEXCEPT  :m_playerNameTo_ (std::move(other_.m_playerNameTo_))
    ,
    m_playerNameFrom_ (std::move(other_.m_playerNameFrom_))
    ,
    m_message_ (std::move(other_.m_message_))
    {
    } 

    Chat& Chat::operator=(Chat&&  other_) OMG_NOEXCEPT {
        Chat tmp(std::move(other_));
        swap(tmp); 
        return *this;
    }
    #endif
    #endif   

    void Chat::swap(Chat& other_)  OMG_NOEXCEPT 
    {
        using std::swap;
        swap(m_playerNameTo_, other_.m_playerNameTo_);
        swap(m_playerNameFrom_, other_.m_playerNameFrom_);
        swap(m_message_, other_.m_message_);
    }  

    bool Chat::operator == (const Chat& other_) const {
        if (m_playerNameTo_ != other_.m_playerNameTo_) {
            return false;
        }
        if (m_playerNameFrom_ != other_.m_playerNameFrom_) {
            return false;
        }
        if (m_message_ != other_.m_message_) {
            return false;
        }
        return true;
    }
    bool Chat::operator != (const Chat& other_) const {
        return !this->operator ==(other_);
    }

    // --- Getters and Setters: -------------------------------------------------
    std::string& RtiWorld::Chat::playerNameTo() OMG_NOEXCEPT {
        return m_playerNameTo_;
    }

    const std::string& RtiWorld::Chat::playerNameTo() const OMG_NOEXCEPT {
        return m_playerNameTo_;
    }

    void RtiWorld::Chat::playerNameTo(const std::string& value) {
        m_playerNameTo_ = value;
    }

    std::string& RtiWorld::Chat::playerNameFrom() OMG_NOEXCEPT {
        return m_playerNameFrom_;
    }

    const std::string& RtiWorld::Chat::playerNameFrom() const OMG_NOEXCEPT {
        return m_playerNameFrom_;
    }

    void RtiWorld::Chat::playerNameFrom(const std::string& value) {
        m_playerNameFrom_ = value;
    }

    std::string& RtiWorld::Chat::message() OMG_NOEXCEPT {
        return m_message_;
    }

    const std::string& RtiWorld::Chat::message() const OMG_NOEXCEPT {
        return m_message_;
    }

    void RtiWorld::Chat::message(const std::string& value) {
        m_message_ = value;
    }

    std::ostream& operator << (std::ostream& o,const Chat& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        o <<"[";
        o << "playerNameTo: " << sample.playerNameTo()<<", ";
        o << "playerNameFrom: " << sample.playerNameFrom()<<", ";
        o << "message: " << sample.message() ;
        o <<"]";
        return o;
    }

} // namespace RtiWorld  

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        template<>
        struct native_type_code<RtiWorld::Chat> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode Chat_g_tc_playerNameTo_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode Chat_g_tc_playerNameFrom_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode Chat_g_tc_message_string = DDS_INITIALIZE_STRING_TYPECODE((500));
                static DDS_TypeCode_Member Chat_g_tc_members[3]=
                {

                    {
                        (char *)"playerNameTo",/* Member name */
                        {
                            0,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_KEY_MEMBER , /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"playerNameFrom",/* Member name */
                        {
                            1,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"message",/* Member name */
                        {
                            2,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode Chat_g_tc =
                {{
                        DDS_TK_STRUCT,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::Chat", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        3, /* Number of members */
                        Chat_g_tc_members, /* Members */
                        DDS_VM_NONE  /* Ignored */         
                    }}; /* Type code for Chat*/

                if (is_initialized) {
                    return &Chat_g_tc;
                }

                Chat_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Chat_g_tc_playerNameTo_string;

                Chat_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Chat_g_tc_playerNameFrom_string;

                Chat_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&Chat_g_tc_message_string;

                is_initialized = RTI_TRUE;

                return &Chat_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::StructType& dynamic_type<RtiWorld::Chat>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::Chat>::get())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<RtiWorld::Chat>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            rti::domain::register_type_plugin(
                participant,
                type_name,
                RtiWorld::ChatPlugin_new,
                RtiWorld::ChatPlugin_delete);
        }

        std::vector<char>& topic_type_support<RtiWorld::Chat>::to_cdr_buffer(
            std::vector<char>& buffer, const RtiWorld::Chat& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = ChatPlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = ChatPlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support<RtiWorld::Chat>::from_cdr_buffer(RtiWorld::Chat& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = ChatPlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create RtiWorld::Chat from cdr buffer");
        }

        void topic_type_support<RtiWorld::Chat>::reset_sample(RtiWorld::Chat& sample) 
        {
            rti::topic::reset_sample(sample.playerNameTo());
            rti::topic::reset_sample(sample.playerNameFrom());
            rti::topic::reset_sample(sample.message());
        }

        void topic_type_support<RtiWorld::Chat>::allocate_sample(RtiWorld::Chat& sample, int, int) 
        {
            rti::topic::allocate_sample(sample.playerNameTo(),  -1, 64);
            rti::topic::allocate_sample(sample.playerNameFrom(),  -1, 64);
            rti::topic::allocate_sample(sample.message(),  -1, 500);
        }

    }
}  

