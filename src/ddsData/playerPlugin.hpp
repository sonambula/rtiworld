

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from player.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef playerPlugin_2094711454_h
#define playerPlugin_2094711454_h

#include "player.hpp"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

namespace RtiWorld {

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct Player
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct Player)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * Player must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct Player.
    */
    typedef  class Player PlayerKeyHolder;

    #define PlayerPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define PlayerPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define PlayerPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define PlayerPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define PlayerPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define PlayerPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define PlayerPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern Player*
    PlayerPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern Player*
    PlayerPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Player*
    PlayerPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    PlayerPluginSupport_copy_data(
        Player *out,
        const Player *in);

    NDDSUSERDllExport extern void 
    PlayerPluginSupport_destroy_data_w_params(
        Player *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    PlayerPluginSupport_destroy_data_ex(
        Player *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    PlayerPluginSupport_destroy_data(
        Player *sample);

    NDDSUSERDllExport extern void 
    PlayerPluginSupport_print_data(
        const Player *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern Player*
    PlayerPluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Player*
    PlayerPluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    PlayerPluginSupport_destroy_key_ex(
        PlayerKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    PlayerPluginSupport_destroy_key(
        PlayerKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    PlayerPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    PlayerPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    PlayerPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    PlayerPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    PlayerPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        Player *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        Player *out,
        const Player *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Player *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Player *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    PlayerPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const Player *sample); 

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        Player **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    PlayerPlugin_deserialize_from_cdr_buffer(
        Player *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    PlayerPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    PlayerPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    PlayerPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    PlayerPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    PlayerPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Player * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    PlayerPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    PlayerPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    PlayerPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Player *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Player * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        Player ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    PlayerPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Player *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        PlayerKeyHolder *key, 
        const Player *instance);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        Player *instance, 
        const PlayerKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const Player *instance);

    NDDSUSERDllExport extern RTIBool 
    PlayerPlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    PlayerPlugin_new(void);

    NDDSUSERDllExport extern void
    PlayerPlugin_delete(struct PRESTypePlugin *);

} /* namespace RtiWorld  */

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* playerPlugin_2094711454_h */

