

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from AttackResult.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>

#include "rti/topic/cdr/Serialization.hpp"

#include "AttackResult.hpp"
#include "AttackResultPlugin.hpp"

#include <rti/util/ostream_operators.hpp>

namespace RtiWorld {
    std::ostream& operator << (std::ostream& o,const AttackResultEnum& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        switch(sample.underlying()){
            case AttackResultEnum::WIN:
            o << "AttackResultEnum::WIN" << " ";
            break;
            case AttackResultEnum::LOSE:
            o << "AttackResultEnum::LOSE" << " ";
            break;
            case AttackResultEnum::PAIR:
            o << "AttackResultEnum::PAIR" << " ";
            break;
            case AttackResultEnum::DIE_ENEMY:
            o << "AttackResultEnum::DIE_ENEMY" << " ";
            break;
        }
        return o;
    }

    // ---- AttackResult: 

    AttackResult::AttackResult() :
        m_result_(RtiWorld::AttackResultEnum::get_default())  {
    }   

    AttackResult::AttackResult (
        const std::string& playerNameFrom,
        const std::string& playerNameTo,
        const RtiWorld::AttackResultEnum& result)
        :
            m_playerNameFrom_( playerNameFrom ),
            m_playerNameTo_( playerNameTo ),
            m_result_( result ) {
    }

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    AttackResult::AttackResult(AttackResult&& other_) OMG_NOEXCEPT  :m_playerNameFrom_ (std::move(other_.m_playerNameFrom_))
    ,
    m_playerNameTo_ (std::move(other_.m_playerNameTo_))
    ,
    m_result_ (std::move(other_.m_result_))
    {
    } 

    AttackResult& AttackResult::operator=(AttackResult&&  other_) OMG_NOEXCEPT {
        AttackResult tmp(std::move(other_));
        swap(tmp); 
        return *this;
    }
    #endif
    #endif   

    void AttackResult::swap(AttackResult& other_)  OMG_NOEXCEPT 
    {
        using std::swap;
        swap(m_playerNameFrom_, other_.m_playerNameFrom_);
        swap(m_playerNameTo_, other_.m_playerNameTo_);
        swap(m_result_, other_.m_result_);
    }  

    bool AttackResult::operator == (const AttackResult& other_) const {
        if (m_playerNameFrom_ != other_.m_playerNameFrom_) {
            return false;
        }
        if (m_playerNameTo_ != other_.m_playerNameTo_) {
            return false;
        }
        if (m_result_ != other_.m_result_) {
            return false;
        }
        return true;
    }
    bool AttackResult::operator != (const AttackResult& other_) const {
        return !this->operator ==(other_);
    }

    // --- Getters and Setters: -------------------------------------------------
    std::string& RtiWorld::AttackResult::playerNameFrom() OMG_NOEXCEPT {
        return m_playerNameFrom_;
    }

    const std::string& RtiWorld::AttackResult::playerNameFrom() const OMG_NOEXCEPT {
        return m_playerNameFrom_;
    }

    void RtiWorld::AttackResult::playerNameFrom(const std::string& value) {
        m_playerNameFrom_ = value;
    }

    std::string& RtiWorld::AttackResult::playerNameTo() OMG_NOEXCEPT {
        return m_playerNameTo_;
    }

    const std::string& RtiWorld::AttackResult::playerNameTo() const OMG_NOEXCEPT {
        return m_playerNameTo_;
    }

    void RtiWorld::AttackResult::playerNameTo(const std::string& value) {
        m_playerNameTo_ = value;
    }

    RtiWorld::AttackResultEnum& RtiWorld::AttackResult::result() OMG_NOEXCEPT {
        return m_result_;
    }

    const RtiWorld::AttackResultEnum& RtiWorld::AttackResult::result() const OMG_NOEXCEPT {
        return m_result_;
    }

    void RtiWorld::AttackResult::result(const RtiWorld::AttackResultEnum& value) {
        m_result_ = value;
    }

    std::ostream& operator << (std::ostream& o,const AttackResult& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        o <<"[";
        o << "playerNameFrom: " << sample.playerNameFrom()<<", ";
        o << "playerNameTo: " << sample.playerNameTo()<<", ";
        o << "result: " << sample.result() ;
        o <<"]";
        return o;
    }

} // namespace RtiWorld  

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        template<>
        struct native_type_code<RtiWorld::AttackResultEnum> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode_Member AttackResultEnum_g_tc_members[4]=
                {

                    {
                        (char *)"WIN",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::AttackResultEnum::WIN, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"LOSE",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::AttackResultEnum::LOSE, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"PAIR",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::AttackResultEnum::PAIR, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"DIE_ENEMY",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::AttackResultEnum::DIE_ENEMY, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode AttackResultEnum_g_tc =
                {{
                        DDS_TK_ENUM,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::AttackResultEnum", /* Name */
                        NULL,     /* Base class type code is assigned later */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        4, /* Number of members */
                        AttackResultEnum_g_tc_members, /* Members */
                        DDS_VM_NONE   /* Type Modifier */        
                    }}; /* Type code for AttackResultEnum*/

                if (is_initialized) {
                    return &AttackResultEnum_g_tc;
                }

                is_initialized = RTI_TRUE;

                return &AttackResultEnum_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::EnumType& dynamic_type<RtiWorld::AttackResultEnum>::get()
        {
            return static_cast<const dds::core::xtypes::EnumType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::AttackResultEnum>::get())));
        }

        template<>
        struct native_type_code<RtiWorld::AttackResult> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode AttackResult_g_tc_playerNameFrom_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode AttackResult_g_tc_playerNameTo_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode_Member AttackResult_g_tc_members[3]=
                {

                    {
                        (char *)"playerNameFrom",/* Member name */
                        {
                            0,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"playerNameTo",/* Member name */
                        {
                            1,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_KEY_MEMBER , /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"result",/* Member name */
                        {
                            2,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode AttackResult_g_tc =
                {{
                        DDS_TK_STRUCT,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::AttackResult", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        3, /* Number of members */
                        AttackResult_g_tc_members, /* Members */
                        DDS_VM_NONE  /* Ignored */         
                    }}; /* Type code for AttackResult*/

                if (is_initialized) {
                    return &AttackResult_g_tc;
                }

                AttackResult_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&AttackResult_g_tc_playerNameFrom_string;

                AttackResult_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&AttackResult_g_tc_playerNameTo_string;

                AttackResult_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&rti::topic::dynamic_type< RtiWorld::AttackResultEnum>::get().native();

                is_initialized = RTI_TRUE;

                return &AttackResult_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::StructType& dynamic_type<RtiWorld::AttackResult>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::AttackResult>::get())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<RtiWorld::AttackResult>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            rti::domain::register_type_plugin(
                participant,
                type_name,
                RtiWorld::AttackResultPlugin_new,
                RtiWorld::AttackResultPlugin_delete);
        }

        std::vector<char>& topic_type_support<RtiWorld::AttackResult>::to_cdr_buffer(
            std::vector<char>& buffer, const RtiWorld::AttackResult& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = AttackResultPlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = AttackResultPlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support<RtiWorld::AttackResult>::from_cdr_buffer(RtiWorld::AttackResult& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = AttackResultPlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create RtiWorld::AttackResult from cdr buffer");
        }

        void topic_type_support<RtiWorld::AttackResult>::reset_sample(RtiWorld::AttackResult& sample) 
        {
            rti::topic::reset_sample(sample.playerNameFrom());
            rti::topic::reset_sample(sample.playerNameTo());
            rti::topic::reset_sample(sample.result());
        }

        void topic_type_support<RtiWorld::AttackResult>::allocate_sample(RtiWorld::AttackResult& sample, int, int) 
        {
            rti::topic::allocate_sample(sample.playerNameFrom(),  -1, 64);
            rti::topic::allocate_sample(sample.playerNameTo(),  -1, 64);
            rti::topic::allocate_sample(sample.result(),  -1, -1);
        }

    }
}  

