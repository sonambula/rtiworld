

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Attack.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef AttackPlugin_30860987_h
#define AttackPlugin_30860987_h

#include "Attack.hpp"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#include "playerPlugin.hpp"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

namespace RtiWorld {

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct Attack
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct Attack)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * Attack must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct Attack.
    */
    typedef  class Attack AttackKeyHolder;

    #define AttackPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define AttackPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define AttackPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define AttackPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define AttackPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define AttackPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define AttackPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern Attack*
    AttackPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern Attack*
    AttackPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Attack*
    AttackPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    AttackPluginSupport_copy_data(
        Attack *out,
        const Attack *in);

    NDDSUSERDllExport extern void 
    AttackPluginSupport_destroy_data_w_params(
        Attack *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    AttackPluginSupport_destroy_data_ex(
        Attack *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    AttackPluginSupport_destroy_data(
        Attack *sample);

    NDDSUSERDllExport extern void 
    AttackPluginSupport_print_data(
        const Attack *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern Attack*
    AttackPluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Attack*
    AttackPluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    AttackPluginSupport_destroy_key_ex(
        AttackKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    AttackPluginSupport_destroy_key(
        AttackKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    AttackPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    AttackPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    AttackPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    AttackPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    AttackPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        Attack *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        Attack *out,
        const Attack *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Attack *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Attack *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const Attack *sample); 

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        Attack **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackPlugin_deserialize_from_cdr_buffer(
        Attack *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    AttackPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    AttackPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    AttackPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    AttackPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    AttackPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Attack * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    AttackPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    AttackPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    AttackPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Attack *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Attack * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        Attack ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Attack *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        AttackKeyHolder *key, 
        const Attack *instance);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        Attack *instance, 
        const AttackKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const Attack *instance);

    NDDSUSERDllExport extern RTIBool 
    AttackPlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    AttackPlugin_new(void);

    NDDSUSERDllExport extern void
    AttackPlugin_delete(struct PRESTypePlugin *);

} /* namespace RtiWorld  */

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* AttackPlugin_30860987_h */

