

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from WeatherStatus.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>

#include "rti/topic/cdr/Serialization.hpp"

#include "WeatherStatus.hpp"
#include "WeatherStatusPlugin.hpp"

#include <rti/util/ostream_operators.hpp>

namespace RtiWorld {
    std::ostream& operator << (std::ostream& o,const WeatherStatusEnum& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        switch(sample.underlying()){
            case WeatherStatusEnum::CLOUDY:
            o << "WeatherStatusEnum::CLOUDY" << " ";
            break;
            case WeatherStatusEnum::SUNNY:
            o << "WeatherStatusEnum::SUNNY" << " ";
            break;
            case WeatherStatusEnum::RAINY:
            o << "WeatherStatusEnum::RAINY" << " ";
            break;
            case WeatherStatusEnum::STORMY:
            o << "WeatherStatusEnum::STORMY" << " ";
            break;
            case WeatherStatusEnum::FOGGY:
            o << "WeatherStatusEnum::FOGGY" << " ";
            break;
        }
        return o;
    }

    // ---- WeatherStatus: 

    WeatherStatus::WeatherStatus() :
        m_status_(RtiWorld::WeatherStatusEnum::get_default()) ,
        m_celsiusDegrees_ (0)  {
    }   

    WeatherStatus::WeatherStatus (
        const RtiWorld::WeatherStatusEnum& status,
        int16_t celsiusDegrees)
        :
            m_status_( status ),
            m_celsiusDegrees_( celsiusDegrees ) {
    }

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    WeatherStatus::WeatherStatus(WeatherStatus&& other_) OMG_NOEXCEPT  :m_status_ (std::move(other_.m_status_))
    ,
    m_celsiusDegrees_ (std::move(other_.m_celsiusDegrees_))
    {
    } 

    WeatherStatus& WeatherStatus::operator=(WeatherStatus&&  other_) OMG_NOEXCEPT {
        WeatherStatus tmp(std::move(other_));
        swap(tmp); 
        return *this;
    }
    #endif
    #endif   

    void WeatherStatus::swap(WeatherStatus& other_)  OMG_NOEXCEPT 
    {
        using std::swap;
        swap(m_status_, other_.m_status_);
        swap(m_celsiusDegrees_, other_.m_celsiusDegrees_);
    }  

    bool WeatherStatus::operator == (const WeatherStatus& other_) const {
        if (m_status_ != other_.m_status_) {
            return false;
        }
        if (m_celsiusDegrees_ != other_.m_celsiusDegrees_) {
            return false;
        }
        return true;
    }
    bool WeatherStatus::operator != (const WeatherStatus& other_) const {
        return !this->operator ==(other_);
    }

    // --- Getters and Setters: -------------------------------------------------
    RtiWorld::WeatherStatusEnum& RtiWorld::WeatherStatus::status() OMG_NOEXCEPT {
        return m_status_;
    }

    const RtiWorld::WeatherStatusEnum& RtiWorld::WeatherStatus::status() const OMG_NOEXCEPT {
        return m_status_;
    }

    void RtiWorld::WeatherStatus::status(const RtiWorld::WeatherStatusEnum& value) {
        m_status_ = value;
    }

    int16_t& RtiWorld::WeatherStatus::celsiusDegrees() OMG_NOEXCEPT {
        return m_celsiusDegrees_;
    }

    const int16_t& RtiWorld::WeatherStatus::celsiusDegrees() const OMG_NOEXCEPT {
        return m_celsiusDegrees_;
    }

    void RtiWorld::WeatherStatus::celsiusDegrees(int16_t value) {
        m_celsiusDegrees_ = value;
    }

    std::ostream& operator << (std::ostream& o,const WeatherStatus& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        o <<"[";
        o << "status: " << sample.status()<<", ";
        o << "celsiusDegrees: " << sample.celsiusDegrees() ;
        o <<"]";
        return o;
    }

} // namespace RtiWorld  

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        template<>
        struct native_type_code<RtiWorld::WeatherStatusEnum> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode_Member WeatherStatusEnum_g_tc_members[5]=
                {

                    {
                        (char *)"CLOUDY",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::WeatherStatusEnum::CLOUDY, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"SUNNY",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::WeatherStatusEnum::SUNNY, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"RAINY",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::WeatherStatusEnum::RAINY, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"STORMY",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::WeatherStatusEnum::STORMY, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"FOGGY",/* Member name */
                        {
                            0, /* Ignored */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        RtiWorld::WeatherStatusEnum::FOGGY, 
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PRIVATE_MEMBER,/* Member visibility */ 

                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode WeatherStatusEnum_g_tc =
                {{
                        DDS_TK_ENUM,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::WeatherStatusEnum", /* Name */
                        NULL,     /* Base class type code is assigned later */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        5, /* Number of members */
                        WeatherStatusEnum_g_tc_members, /* Members */
                        DDS_VM_NONE   /* Type Modifier */        
                    }}; /* Type code for WeatherStatusEnum*/

                if (is_initialized) {
                    return &WeatherStatusEnum_g_tc;
                }

                is_initialized = RTI_TRUE;

                return &WeatherStatusEnum_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::EnumType& dynamic_type<RtiWorld::WeatherStatusEnum>::get()
        {
            return static_cast<const dds::core::xtypes::EnumType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::WeatherStatusEnum>::get())));
        }

        template<>
        struct native_type_code<RtiWorld::WeatherStatus> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode_Member WeatherStatus_g_tc_members[2]=
                {

                    {
                        (char *)"status",/* Member name */
                        {
                            0,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"celsiusDegrees",/* Member name */
                        {
                            1,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode WeatherStatus_g_tc =
                {{
                        DDS_TK_STRUCT,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::WeatherStatus", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        2, /* Number of members */
                        WeatherStatus_g_tc_members, /* Members */
                        DDS_VM_NONE  /* Ignored */         
                    }}; /* Type code for WeatherStatus*/

                if (is_initialized) {
                    return &WeatherStatus_g_tc;
                }

                WeatherStatus_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&rti::topic::dynamic_type< RtiWorld::WeatherStatusEnum>::get().native();

                WeatherStatus_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_short;

                is_initialized = RTI_TRUE;

                return &WeatherStatus_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::StructType& dynamic_type<RtiWorld::WeatherStatus>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::WeatherStatus>::get())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<RtiWorld::WeatherStatus>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            rti::domain::register_type_plugin(
                participant,
                type_name,
                RtiWorld::WeatherStatusPlugin_new,
                RtiWorld::WeatherStatusPlugin_delete);
        }

        std::vector<char>& topic_type_support<RtiWorld::WeatherStatus>::to_cdr_buffer(
            std::vector<char>& buffer, const RtiWorld::WeatherStatus& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = WeatherStatusPlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = WeatherStatusPlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support<RtiWorld::WeatherStatus>::from_cdr_buffer(RtiWorld::WeatherStatus& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = WeatherStatusPlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create RtiWorld::WeatherStatus from cdr buffer");
        }

        void topic_type_support<RtiWorld::WeatherStatus>::reset_sample(RtiWorld::WeatherStatus& sample) 
        {
            rti::topic::reset_sample(sample.status());
            rti::topic::reset_sample(sample.celsiusDegrees());
        }

        void topic_type_support<RtiWorld::WeatherStatus>::allocate_sample(RtiWorld::WeatherStatus& sample, int, int) 
        {
            rti::topic::allocate_sample(sample.status(),  -1, -1);
        }

    }
}  

