

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from player.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef player_2094711454_hpp
#define player_2094711454_hpp

#include <iosfwd>

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/core/BoundedSequence.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "dds/core/External.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

namespace RtiWorld {

    class NDDSUSERDllExport Player {

      public:
        Player();
        Player(
            uint16_t level,
            uint16_t live,
            const std::string& name);

        #ifdef RTI_CXX11_RVALUE_REFERENCES
        #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
        Player (Player&&) = default;
        Player& operator=(Player&&) = default;
        Player& operator=(const Player&) = default;
        Player(const Player&) = default;
        #else
        Player(Player&& other_) OMG_NOEXCEPT;  
        Player& operator=(Player&&  other_) OMG_NOEXCEPT;
        #endif
        #endif 

        uint16_t& level() OMG_NOEXCEPT; 
        const uint16_t& level() const OMG_NOEXCEPT;
        void level(uint16_t value);

        uint16_t& live() OMG_NOEXCEPT; 
        const uint16_t& live() const OMG_NOEXCEPT;
        void live(uint16_t value);

        std::string& name() OMG_NOEXCEPT; 
        const std::string& name() const OMG_NOEXCEPT;
        void name(const std::string& value);

        bool operator == (const Player& other_) const;
        bool operator != (const Player& other_) const;

        void swap(Player& other_) OMG_NOEXCEPT ;

      private:

        uint16_t m_level_;
        uint16_t m_live_;
        std::string m_name_;

    };

    inline void swap(Player& a, Player& b)  OMG_NOEXCEPT 
    {
        a.swap(b);
    }

    NDDSUSERDllExport std::ostream& operator<<(std::ostream& o, const Player& sample);

} // namespace RtiWorld  
namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<RtiWorld::Player> {
            NDDSUSERDllExport static std::string value() {
                return "RtiWorld::Player";
            }
        };

        template<>
        struct is_topic_type<RtiWorld::Player> : public dds::core::true_type {};

        template<>
        struct topic_type_support<RtiWorld::Player> {
            NDDSUSERDllExport 
            static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport 
            static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const RtiWorld::Player& sample);

            NDDSUSERDllExport 
            static void from_cdr_buffer(RtiWorld::Player& sample, const std::vector<char>& buffer);

            NDDSUSERDllExport 
            static void reset_sample(RtiWorld::Player& sample);

            NDDSUSERDllExport 
            static void allocate_sample(RtiWorld::Player& sample, int, int);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::STL;
        };

    }
}

namespace rti { 
    namespace topic {

        template<>
        struct dynamic_type<RtiWorld::Player> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template <>
        struct extensibility<RtiWorld::Player> {
            static const dds::core::xtypes::ExtensibilityKind::type kind =
            dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // player_2094711454_hpp

