

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from AttackResult.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef AttackResult_1066321597_hpp
#define AttackResult_1066321597_hpp

#include <iosfwd>

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/core/BoundedSequence.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "dds/core/External.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif
#include "player.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif
namespace RtiWorld {
    struct AttackResultEnum_def {
        enum type {
            WIN,      
            LOSE,      
            PAIR,      
            DIE_ENEMY     
        };
        static type get_default(){ return WIN;}
    };

    typedef dds::core::safe_enum<AttackResultEnum_def> AttackResultEnum;
    NDDSUSERDllExport std::ostream& operator << (std::ostream& o,const AttackResultEnum& sample);

    class NDDSUSERDllExport AttackResult {

      public:
        AttackResult();
        AttackResult(
            const std::string& playerNameFrom,
            const std::string& playerNameTo,
            const RtiWorld::AttackResultEnum& result);

        #ifdef RTI_CXX11_RVALUE_REFERENCES
        #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
        AttackResult (AttackResult&&) = default;
        AttackResult& operator=(AttackResult&&) = default;
        AttackResult& operator=(const AttackResult&) = default;
        AttackResult(const AttackResult&) = default;
        #else
        AttackResult(AttackResult&& other_) OMG_NOEXCEPT;  
        AttackResult& operator=(AttackResult&&  other_) OMG_NOEXCEPT;
        #endif
        #endif 

        std::string& playerNameFrom() OMG_NOEXCEPT; 
        const std::string& playerNameFrom() const OMG_NOEXCEPT;
        void playerNameFrom(const std::string& value);

        std::string& playerNameTo() OMG_NOEXCEPT; 
        const std::string& playerNameTo() const OMG_NOEXCEPT;
        void playerNameTo(const std::string& value);

        RtiWorld::AttackResultEnum& result() OMG_NOEXCEPT; 
        const RtiWorld::AttackResultEnum& result() const OMG_NOEXCEPT;
        void result(const RtiWorld::AttackResultEnum& value);

        bool operator == (const AttackResult& other_) const;
        bool operator != (const AttackResult& other_) const;

        void swap(AttackResult& other_) OMG_NOEXCEPT ;

      private:

        std::string m_playerNameFrom_;
        std::string m_playerNameTo_;
        RtiWorld::AttackResultEnum m_result_;

    };

    inline void swap(AttackResult& a, AttackResult& b)  OMG_NOEXCEPT 
    {
        a.swap(b);
    }

    NDDSUSERDllExport std::ostream& operator<<(std::ostream& o, const AttackResult& sample);

} // namespace RtiWorld  
namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<RtiWorld::AttackResult> {
            NDDSUSERDllExport static std::string value() {
                return "RtiWorld::AttackResult";
            }
        };

        template<>
        struct is_topic_type<RtiWorld::AttackResult> : public dds::core::true_type {};

        template<>
        struct topic_type_support<RtiWorld::AttackResult> {
            NDDSUSERDllExport 
            static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport 
            static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const RtiWorld::AttackResult& sample);

            NDDSUSERDllExport 
            static void from_cdr_buffer(RtiWorld::AttackResult& sample, const std::vector<char>& buffer);

            NDDSUSERDllExport 
            static void reset_sample(RtiWorld::AttackResult& sample);

            NDDSUSERDllExport 
            static void allocate_sample(RtiWorld::AttackResult& sample, int, int);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::STL;
        };

    }
}

namespace rti { 
    namespace topic {

        template<>
        struct dynamic_type<RtiWorld::AttackResultEnum> {
            typedef dds::core::xtypes::EnumType type;
            NDDSUSERDllExport static const dds::core::xtypes::EnumType& get();
        };

        template <>
        struct extensibility<RtiWorld::AttackResultEnum> {
            static const dds::core::xtypes::ExtensibilityKind::type kind =
            dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

        template<>
        struct dynamic_type<RtiWorld::AttackResult> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template <>
        struct extensibility<RtiWorld::AttackResult> {
            static const dds::core::xtypes::ExtensibilityKind::type kind =
            dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // AttackResult_1066321597_hpp

