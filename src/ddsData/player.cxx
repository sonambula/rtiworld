

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from player.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>

#include "rti/topic/cdr/Serialization.hpp"

#include "player.hpp"
#include "playerPlugin.hpp"

#include <rti/util/ostream_operators.hpp>

namespace RtiWorld {

    // ---- Player: 

    Player::Player() :
        m_level_ (0) ,
        m_live_ (0)  {
    }   

    Player::Player (
        uint16_t level,
        uint16_t live,
        const std::string& name)
        :
            m_level_( level ),
            m_live_( live ),
            m_name_( name ) {
    }

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Player::Player(Player&& other_) OMG_NOEXCEPT  :m_level_ (std::move(other_.m_level_))
    ,
    m_live_ (std::move(other_.m_live_))
    ,
    m_name_ (std::move(other_.m_name_))
    {
    } 

    Player& Player::operator=(Player&&  other_) OMG_NOEXCEPT {
        Player tmp(std::move(other_));
        swap(tmp); 
        return *this;
    }
    #endif
    #endif   

    void Player::swap(Player& other_)  OMG_NOEXCEPT 
    {
        using std::swap;
        swap(m_level_, other_.m_level_);
        swap(m_live_, other_.m_live_);
        swap(m_name_, other_.m_name_);
    }  

    bool Player::operator == (const Player& other_) const {
        if (m_level_ != other_.m_level_) {
            return false;
        }
        if (m_live_ != other_.m_live_) {
            return false;
        }
        if (m_name_ != other_.m_name_) {
            return false;
        }
        return true;
    }
    bool Player::operator != (const Player& other_) const {
        return !this->operator ==(other_);
    }

    // --- Getters and Setters: -------------------------------------------------
    uint16_t& RtiWorld::Player::level() OMG_NOEXCEPT {
        return m_level_;
    }

    const uint16_t& RtiWorld::Player::level() const OMG_NOEXCEPT {
        return m_level_;
    }

    void RtiWorld::Player::level(uint16_t value) {
        m_level_ = value;
    }

    uint16_t& RtiWorld::Player::live() OMG_NOEXCEPT {
        return m_live_;
    }

    const uint16_t& RtiWorld::Player::live() const OMG_NOEXCEPT {
        return m_live_;
    }

    void RtiWorld::Player::live(uint16_t value) {
        m_live_ = value;
    }

    std::string& RtiWorld::Player::name() OMG_NOEXCEPT {
        return m_name_;
    }

    const std::string& RtiWorld::Player::name() const OMG_NOEXCEPT {
        return m_name_;
    }

    void RtiWorld::Player::name(const std::string& value) {
        m_name_ = value;
    }

    std::ostream& operator << (std::ostream& o,const Player& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        o <<"[";
        o << "level: " << sample.level()<<", ";
        o << "live: " << sample.live()<<", ";
        o << "name: " << sample.name() ;
        o <<"]";
        return o;
    }

} // namespace RtiWorld  

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        template<>
        struct native_type_code<RtiWorld::Player> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode Player_g_tc_name_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode_Member Player_g_tc_members[3]=
                {

                    {
                        (char *)"level",/* Member name */
                        {
                            0,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"live",/* Member name */
                        {
                            1,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"name",/* Member name */
                        {
                            2,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_KEY_MEMBER , /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode Player_g_tc =
                {{
                        DDS_TK_STRUCT,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::Player", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        3, /* Number of members */
                        Player_g_tc_members, /* Members */
                        DDS_VM_NONE  /* Ignored */         
                    }}; /* Type code for Player*/

                if (is_initialized) {
                    return &Player_g_tc;
                }

                Player_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_ushort;

                Player_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_ushort;

                Player_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&Player_g_tc_name_string;

                is_initialized = RTI_TRUE;

                return &Player_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::StructType& dynamic_type<RtiWorld::Player>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::Player>::get())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<RtiWorld::Player>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            rti::domain::register_type_plugin(
                participant,
                type_name,
                RtiWorld::PlayerPlugin_new,
                RtiWorld::PlayerPlugin_delete);
        }

        std::vector<char>& topic_type_support<RtiWorld::Player>::to_cdr_buffer(
            std::vector<char>& buffer, const RtiWorld::Player& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = PlayerPlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = PlayerPlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support<RtiWorld::Player>::from_cdr_buffer(RtiWorld::Player& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = PlayerPlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create RtiWorld::Player from cdr buffer");
        }

        void topic_type_support<RtiWorld::Player>::reset_sample(RtiWorld::Player& sample) 
        {
            rti::topic::reset_sample(sample.level());
            rti::topic::reset_sample(sample.live());
            rti::topic::reset_sample(sample.name());
        }

        void topic_type_support<RtiWorld::Player>::allocate_sample(RtiWorld::Player& sample, int, int) 
        {
            rti::topic::allocate_sample(sample.name(),  -1, 64);
        }

    }
}  

