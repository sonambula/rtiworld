

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Attack.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>

#include "rti/topic/cdr/Serialization.hpp"

#include "Attack.hpp"
#include "AttackPlugin.hpp"

#include <rti/util/ostream_operators.hpp>

namespace RtiWorld {

    // ---- Attack: 

    Attack::Attack()  {
    }   

    Attack::Attack (
        const std::string& playerNameFrom,
        const std::string& playerNameTo,
        const RtiWorld::Player& attacker)
        :
            m_playerNameFrom_( playerNameFrom ),
            m_playerNameTo_( playerNameTo ),
            m_attacker_( attacker ) {
    }

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Attack::Attack(Attack&& other_) OMG_NOEXCEPT  :m_playerNameFrom_ (std::move(other_.m_playerNameFrom_))
    ,
    m_playerNameTo_ (std::move(other_.m_playerNameTo_))
    ,
    m_attacker_ (std::move(other_.m_attacker_))
    {
    } 

    Attack& Attack::operator=(Attack&&  other_) OMG_NOEXCEPT {
        Attack tmp(std::move(other_));
        swap(tmp); 
        return *this;
    }
    #endif
    #endif   

    void Attack::swap(Attack& other_)  OMG_NOEXCEPT 
    {
        using std::swap;
        swap(m_playerNameFrom_, other_.m_playerNameFrom_);
        swap(m_playerNameTo_, other_.m_playerNameTo_);
        swap(m_attacker_, other_.m_attacker_);
    }  

    bool Attack::operator == (const Attack& other_) const {
        if (m_playerNameFrom_ != other_.m_playerNameFrom_) {
            return false;
        }
        if (m_playerNameTo_ != other_.m_playerNameTo_) {
            return false;
        }
        if (m_attacker_ != other_.m_attacker_) {
            return false;
        }
        return true;
    }
    bool Attack::operator != (const Attack& other_) const {
        return !this->operator ==(other_);
    }

    // --- Getters and Setters: -------------------------------------------------
    std::string& RtiWorld::Attack::playerNameFrom() OMG_NOEXCEPT {
        return m_playerNameFrom_;
    }

    const std::string& RtiWorld::Attack::playerNameFrom() const OMG_NOEXCEPT {
        return m_playerNameFrom_;
    }

    void RtiWorld::Attack::playerNameFrom(const std::string& value) {
        m_playerNameFrom_ = value;
    }

    std::string& RtiWorld::Attack::playerNameTo() OMG_NOEXCEPT {
        return m_playerNameTo_;
    }

    const std::string& RtiWorld::Attack::playerNameTo() const OMG_NOEXCEPT {
        return m_playerNameTo_;
    }

    void RtiWorld::Attack::playerNameTo(const std::string& value) {
        m_playerNameTo_ = value;
    }

    RtiWorld::Player& RtiWorld::Attack::attacker() OMG_NOEXCEPT {
        return m_attacker_;
    }

    const RtiWorld::Player& RtiWorld::Attack::attacker() const OMG_NOEXCEPT {
        return m_attacker_;
    }

    void RtiWorld::Attack::attacker(const RtiWorld::Player& value) {
        m_attacker_ = value;
    }

    std::ostream& operator << (std::ostream& o,const Attack& sample)
    {
        rti::util::StreamFlagSaver flag_saver (o);
        o <<"[";
        o << "playerNameFrom: " << sample.playerNameFrom()<<", ";
        o << "playerNameTo: " << sample.playerNameTo()<<", ";
        o << "attacker: " << sample.attacker() ;
        o <<"]";
        return o;
    }

} // namespace RtiWorld  

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        template<>
        struct native_type_code<RtiWorld::Attack> {
            static DDS_TypeCode * get()
            {
                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode Attack_g_tc_playerNameFrom_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode Attack_g_tc_playerNameTo_string = DDS_INITIALIZE_STRING_TYPECODE((64));
                static DDS_TypeCode_Member Attack_g_tc_members[3]=
                {

                    {
                        (char *)"playerNameFrom",/* Member name */
                        {
                            0,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"playerNameTo",/* Member name */
                        {
                            1,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_KEY_MEMBER , /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }, 
                    {
                        (char *)"attacker",/* Member name */
                        {
                            2,/* Representation ID */          
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL/* Ignored */
                    }
                };

                static DDS_TypeCode Attack_g_tc =
                {{
                        DDS_TK_STRUCT,/* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"RtiWorld::Attack", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        3, /* Number of members */
                        Attack_g_tc_members, /* Members */
                        DDS_VM_NONE  /* Ignored */         
                    }}; /* Type code for Attack*/

                if (is_initialized) {
                    return &Attack_g_tc;
                }

                Attack_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Attack_g_tc_playerNameFrom_string;

                Attack_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Attack_g_tc_playerNameTo_string;

                Attack_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&rti::topic::dynamic_type< RtiWorld::Player>::get().native();

                is_initialized = RTI_TRUE;

                return &Attack_g_tc;
            }
        }; // native_type_code

        const dds::core::xtypes::StructType& dynamic_type<RtiWorld::Attack>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(native_type_code<RtiWorld::Attack>::get())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<RtiWorld::Attack>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            rti::domain::register_type_plugin(
                participant,
                type_name,
                RtiWorld::AttackPlugin_new,
                RtiWorld::AttackPlugin_delete);
        }

        std::vector<char>& topic_type_support<RtiWorld::Attack>::to_cdr_buffer(
            std::vector<char>& buffer, const RtiWorld::Attack& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = AttackPlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = AttackPlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support<RtiWorld::Attack>::from_cdr_buffer(RtiWorld::Attack& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = AttackPlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create RtiWorld::Attack from cdr buffer");
        }

        void topic_type_support<RtiWorld::Attack>::reset_sample(RtiWorld::Attack& sample) 
        {
            rti::topic::reset_sample(sample.playerNameFrom());
            rti::topic::reset_sample(sample.playerNameTo());
            rti::topic::reset_sample(sample.attacker());
        }

        void topic_type_support<RtiWorld::Attack>::allocate_sample(RtiWorld::Attack& sample, int, int) 
        {
            rti::topic::allocate_sample(sample.playerNameFrom(),  -1, 64);
            rti::topic::allocate_sample(sample.playerNameTo(),  -1, 64);
            rti::topic::allocate_sample(sample.attacker(),  -1, -1);
        }

    }
}  

