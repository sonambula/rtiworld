

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from AttackResult.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef AttackResultPlugin_1066321597_h
#define AttackResultPlugin_1066321597_h

#include "AttackResult.hpp"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#include "playerPlugin.hpp"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

namespace RtiWorld {

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    AttackResultEnumPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const AttackResultEnum *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackResultEnumPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        AttackResultEnum *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackResultEnumPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    AttackResultEnumPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    AttackResultEnumPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    AttackResultEnumPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    AttackResultEnumPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const AttackResultEnum * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern unsigned int 
    AttackResultEnumPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    AttackResultEnumPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    AttackResultEnumPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const AttackResultEnum *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackResultEnumPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        AttackResultEnum * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackResultEnumPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        AttackResultEnum *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* ----------------------------------------------------------------------------
    Support functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern void
    AttackResultEnumPluginSupport_print_data(
        const AttackResultEnum *sample, const char *desc, int indent_level);

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct AttackResult
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct AttackResult)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * AttackResult must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct AttackResult.
    */
    typedef  class AttackResult AttackResultKeyHolder;

    #define AttackResultPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define AttackResultPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define AttackResultPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define AttackResultPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define AttackResultPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define AttackResultPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define AttackResultPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern AttackResult*
    AttackResultPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern AttackResult*
    AttackResultPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern AttackResult*
    AttackResultPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPluginSupport_copy_data(
        AttackResult *out,
        const AttackResult *in);

    NDDSUSERDllExport extern void 
    AttackResultPluginSupport_destroy_data_w_params(
        AttackResult *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    AttackResultPluginSupport_destroy_data_ex(
        AttackResult *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    AttackResultPluginSupport_destroy_data(
        AttackResult *sample);

    NDDSUSERDllExport extern void 
    AttackResultPluginSupport_print_data(
        const AttackResult *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern AttackResult*
    AttackResultPluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern AttackResult*
    AttackResultPluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    AttackResultPluginSupport_destroy_key_ex(
        AttackResultKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    AttackResultPluginSupport_destroy_key(
        AttackResultKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    AttackResultPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    AttackResultPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    AttackResultPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    AttackResultPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    AttackResultPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult *out,
        const AttackResult *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const AttackResult *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackResultPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const AttackResult *sample); 

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackResultPlugin_deserialize_from_cdr_buffer(
        AttackResult *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    AttackResultPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    AttackResultPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    AttackResultPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    AttackResultPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    AttackResultPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const AttackResult * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    AttackResultPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    AttackResultPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    AttackResultPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const AttackResult *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    AttackResultPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        AttackResultKeyHolder *key, 
        const AttackResult *instance);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        AttackResult *instance, 
        const AttackResultKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const AttackResult *instance);

    NDDSUSERDllExport extern RTIBool 
    AttackResultPlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    AttackResultPlugin_new(void);

    NDDSUSERDllExport extern void
    AttackResultPlugin_delete(struct PRESTypePlugin *);

} /* namespace RtiWorld  */

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* AttackResultPlugin_1066321597_h */

