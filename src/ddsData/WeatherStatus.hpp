

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from WeatherStatus.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef WeatherStatus_1902446338_hpp
#define WeatherStatus_1902446338_hpp

#include <iosfwd>

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/core/BoundedSequence.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "dds/core/External.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

namespace RtiWorld {
    struct WeatherStatusEnum_def {
        enum type {
            CLOUDY,      
            SUNNY,      
            RAINY,      
            STORMY,      
            FOGGY     
        };
        static type get_default(){ return CLOUDY;}
    };

    typedef dds::core::safe_enum<WeatherStatusEnum_def> WeatherStatusEnum;
    NDDSUSERDllExport std::ostream& operator << (std::ostream& o,const WeatherStatusEnum& sample);

    class NDDSUSERDllExport WeatherStatus {

      public:
        WeatherStatus();
        WeatherStatus(
            const RtiWorld::WeatherStatusEnum& status,
            int16_t celsiusDegrees);

        #ifdef RTI_CXX11_RVALUE_REFERENCES
        #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
        WeatherStatus (WeatherStatus&&) = default;
        WeatherStatus& operator=(WeatherStatus&&) = default;
        WeatherStatus& operator=(const WeatherStatus&) = default;
        WeatherStatus(const WeatherStatus&) = default;
        #else
        WeatherStatus(WeatherStatus&& other_) OMG_NOEXCEPT;  
        WeatherStatus& operator=(WeatherStatus&&  other_) OMG_NOEXCEPT;
        #endif
        #endif 

        RtiWorld::WeatherStatusEnum& status() OMG_NOEXCEPT; 
        const RtiWorld::WeatherStatusEnum& status() const OMG_NOEXCEPT;
        void status(const RtiWorld::WeatherStatusEnum& value);

        int16_t& celsiusDegrees() OMG_NOEXCEPT; 
        const int16_t& celsiusDegrees() const OMG_NOEXCEPT;
        void celsiusDegrees(int16_t value);

        bool operator == (const WeatherStatus& other_) const;
        bool operator != (const WeatherStatus& other_) const;

        void swap(WeatherStatus& other_) OMG_NOEXCEPT ;

      private:

        RtiWorld::WeatherStatusEnum m_status_;
        int16_t m_celsiusDegrees_;

    };

    inline void swap(WeatherStatus& a, WeatherStatus& b)  OMG_NOEXCEPT 
    {
        a.swap(b);
    }

    NDDSUSERDllExport std::ostream& operator<<(std::ostream& o, const WeatherStatus& sample);

} // namespace RtiWorld  
namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<RtiWorld::WeatherStatus> {
            NDDSUSERDllExport static std::string value() {
                return "RtiWorld::WeatherStatus";
            }
        };

        template<>
        struct is_topic_type<RtiWorld::WeatherStatus> : public dds::core::true_type {};

        template<>
        struct topic_type_support<RtiWorld::WeatherStatus> {
            NDDSUSERDllExport 
            static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport 
            static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const RtiWorld::WeatherStatus& sample);

            NDDSUSERDllExport 
            static void from_cdr_buffer(RtiWorld::WeatherStatus& sample, const std::vector<char>& buffer);

            NDDSUSERDllExport 
            static void reset_sample(RtiWorld::WeatherStatus& sample);

            NDDSUSERDllExport 
            static void allocate_sample(RtiWorld::WeatherStatus& sample, int, int);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::STL;
        };

    }
}

namespace rti { 
    namespace topic {

        template<>
        struct dynamic_type<RtiWorld::WeatherStatusEnum> {
            typedef dds::core::xtypes::EnumType type;
            NDDSUSERDllExport static const dds::core::xtypes::EnumType& get();
        };

        template <>
        struct extensibility<RtiWorld::WeatherStatusEnum> {
            static const dds::core::xtypes::ExtensibilityKind::type kind =
            dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

        template<>
        struct dynamic_type<RtiWorld::WeatherStatus> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template <>
        struct extensibility<RtiWorld::WeatherStatus> {
            static const dds::core::xtypes::ExtensibilityKind::type kind =
            dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // WeatherStatus_1902446338_hpp

